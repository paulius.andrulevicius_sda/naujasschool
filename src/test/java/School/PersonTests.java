package School;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonTests {
    // act given when then
    //
    @Test
    void getAge_when_person_bornToday_returns_0(){
        // Arrange
        Person person = new Person("a", "a", LocalDate.now(), 0, 1, Gender.MALE);

        // Act
        int age = person.getAge();
        assertEquals(0, age);
    }

//    void getWight_when_person_weight
}
